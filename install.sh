#!/bin/bash
echo "====Downloading NodeJS===="

curl -O https://nodejs.org/dist/v10.15.0/node-v10.15.0.pkg

echo
echo "====Installing NodeJS===="
echo

open node-v10.15.0.pkg

read -p "Press ENTER when NodesJS setup completes..."

rm node-v10.15.0.pkg

echo
echo "====Clone Presto Core Source==="
echo

git clone git@bitbucket.org:jcollinsnw/presto2.git presto

echo
echo "====Install Presto Core Dependencies==="
echo

cd presto
npm install

echo
echo "====Clone Peerpal Source==="
echo

cd scripts/frontend/src/apps/
git clone git@bitbucket.org:jcollinsnw/presto2-peerpal.git peerpal

echo
echo "====Install Peerpal Dependencies==="
echo

npm install

echo
echo
echo
echo "Install complete. Change to presto directory and run \"npm run dev\" to start the dev server:"
echo "  cd presto"
echo "  npm run dev"