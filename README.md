## 1. Run Install Script
Open a terminal and run this command:

    curl https://bitbucket.org/jcollinsnw/presto-install/raw/28a73dc632721446d8692a677a76d4ef49f82d26/install.sh | bash

## 2. Start the Development Server
    cd presto
    npm run dev

## 3. Authenticate the Development Server
1. Go to the Canvas test course.
2. Navigate to Modules and find the Presto Link that points to your localhost dev environment. Or create this.
3. Launch the module by clicking and then press the load in new window button.
4. This will authenticate you to allow your development environment to make Canvas API calls.

## 4. Change Course ID
By default Canvas API calls will use the development course. We may need to switch this context to a production course.

You should see a red bar at the top of the screen. If not use arrow keys and letters to type the Konami Code as follows to toggle the debug bar:

    ↑↑↓↓←→←→BA
    
You can copy any Course ID found in Canvas by navigating to that course and copy the 5 digit number at the end of the URL and paste it into this box. Then refresh the page to change to that course.